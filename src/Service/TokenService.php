<?php

namespace App\Service;

use App\Entity\Participant;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class TokenService
{
    private JWTTokenManagerInterface $JWTTokenManager;
    private JWTEncoderInterface $JWTEncoder;
    private ApiService $apiService;

    public function __construct(JWTTokenManagerInterface $JWTTokenManager, JWTEncoderInterface $JWTEncoder, ApiService $apiService)
    {
        $this->JWTTokenManager = $JWTTokenManager;
        $this->JWTEncoder = $JWTEncoder;
        $this->apiService = $apiService;
    }

    public function createToken(Participant $participant): string
    {
        return $this->JWTTokenManager->create($participant);
    }

    public function checkTokenValidity(string $token): array
    {
        $payload = null;

        try {
            $payload = $this->JWTEncoder->decode($token);
            $responseCode = Response::HTTP_ACCEPTED;
        } catch (InvalidTokenException|ExpiredTokenException|JWTDecodeFailureException) {
            $responseCode = Response::HTTP_UNAUTHORIZED;
        }

        return [
            "payload" => $payload,
            "responseCode" => $responseCode,
        ];
    }

    public function compareEmailFromToken(string $email, string $token)
    {
        try {
            $emailFromToken = $this->JWTEncoder->decode($token);
            if ($email === $emailFromToken["username"]) {
                return true;
            } else {
                return false;
            }
        } catch (JWTDecodeFailureException) {
            return false;
        }
    }
}