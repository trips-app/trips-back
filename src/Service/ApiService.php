<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

class ApiService
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Créé une réponse JSON prête à être envoyé au front
     *
     * @param $dataToSend
     * @param int $statusCode
     * @return JsonResponse
     */
    public function createJsonResponse($dataToSend, int $statusCode): JsonResponse
    {
        $data = ["data" => $dataToSend];
        // Sérialise la réponse en JSON
        $jsonResponse = $this->serializer->serialize($data, 'json');
        // Renvoie la réponse JSON avec le contenu de l'objet et le statut de la réponse
        return new JsonResponse($jsonResponse, $statusCode, [], true);
    }
}