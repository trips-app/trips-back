<?php

namespace App\Controller;

use App\Entity\Trip;
use App\Repository\ParticipantRepository;
use App\Repository\PlaceRepository;
use App\Repository\StateRepository;
use App\Repository\TripRepository;
use App\Service\ApiService;
use App\Service\TokenService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TripController extends AbstractController
{
    private TokenService $tokenService;
    private ApiService $apiService;

    public function __construct(TokenService $tokenService, ApiService $apiService)
    {
        $this->tokenService = $tokenService;
        $this->apiService = $apiService;
    }


    /**
     * Envoie les informations de la sortie spécifiée dans la requête
     *
     * @param Request $request
     * @param TripRepository $tripRepository
     * @return JsonResponse
     */
    #[Route('/api/trip/detail', name: 'api_trip_detail ', methods: ['GET'])]
    public function detail(Request $request, TripRepository $tripRepository): JsonResponse
    {
        // Décode les données du front
        $data = json_decode($request->getContent(), true);

        $trip = $tripRepository->findBy(["name" => $data["tripName"]]);

        return $this->apiService->createJsonResponse($trip[0]->toArray(), Response::HTTP_OK);
    }


    #[Route('/api/trip/list', name: 'api_trip_list', methods: ['GET'])]
    public function list(TripRepository $tripRepository): JsonResponse
    {
        // Récupère la liste des voyages
        $tripList = $tripRepository->findAll();

        // Récupère la liste des voyages sans la boucle infini
        $tripListCleaned = [];
        foreach ($tripList as $trip) {
            $tripCleaned = $trip->toArray();
            $tripListCleaned[] = $tripCleaned;
        }

        return $this->apiService->createJsonResponse($tripListCleaned, Response::HTTP_OK);
    }


    /**
     * Créé une sortie pour le participant qui envoie la requête
     *
     * @param Request $request
     * @param StateRepository $stateRepository
     * @param PlaceRepository $placeRepository
     * @param ParticipantRepository $participantRepository
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     * @throws \Exception
     */
    #[Route('/api/add-trip', name: 'api_add-trip', methods: ['POST', 'OPTIONS'])]
    public function create(Request $request, StateRepository $stateRepository, PlaceRepository $placeRepository, ParticipantRepository $participantRepository, EntityManagerInterface $entityManager): JsonResponse
    {
        // Décode les données du front
        $data = json_decode($request->getContent(), true);
        $tripDetails = $data['tripDetails'];

        // Vérifie que le participant ne peut créer une sortie qu'avec son profil
        if (!$this->tokenService->compareEmailFromToken($data['email'], $data['token'])) {
            return $this->apiService->createJsonResponse("Vous ne pouvez pas créer une sortie à la place d'un autre participant", Response::HTTP_UNAUTHORIZED);
        }

        $place = $placeRepository->findBy(["name" => $tripDetails["place"]]);
        $state = $stateRepository->findBy(["libelle" => "Created"]);
        $organizer = $participantRepository->findBy(["email" => $data['email']]);

        $trip = new Trip();
        $trip
            ->setName($tripDetails['name'])
            ->setStartTime(new \DateTime($tripDetails['startTime']))
            ->setDuration($tripDetails['duration'])
            ->setPlace($place[0])
            ->setRegistrationDeadline(new \DateTime($tripDetails['registrationDeadline']))
            ->setMaxRegistrations($tripDetails['maxRegistrations'])
            ->setInformation($tripDetails['information'])
            ->setState($state[0])
            ->setOrganizer($organizer[0])
            ->setSiteOrganization($organizer[0]->getSite());

        $entityManager->persist($trip);
        $entityManager->flush();

        return $this->apiService->createJsonResponse($trip->toArray(), Response::HTTP_OK);
    }


    /**
     * Modifie la sortie spécifiée dans la requête
     *
     * @param Request $request
     * @param TripRepository $tripRepository
     * @param EntityManagerInterface $entityManager
     * @param StateRepository $stateRepository
     * @return JsonResponse
     */
    #[Route('/api/trip/edit', name: 'api_trip_edit', methods: ['POST', 'OPTIONS'])]
    public function edit(Request $request, TripRepository $tripRepository, EntityManagerInterface $entityManager, StateRepository $stateRepository): JsonResponse
    {
        // Décode les données du front
        $data = json_decode($request->getContent(), true);

        $tripDetails = $data['tripDetails'];
        $trip = $tripRepository->findBy(['name' => $data['tripName']]);
        $organizer = $trip[0]->getOrganizer();
        $state = $stateRepository->findBy(['libelle' => $tripDetails['state']]);

        // Vérifie que le participant ne peut modifier que ses sorties
        if (!$this->tokenService->compareEmailFromToken($organizer->getEmail(), $data['token'])) {
            return $this->apiService->createJsonResponse("Cette sortie ne vous appartient pas", Response::HTTP_UNAUTHORIZED);
        }

        $trip[0]
            ->setName($tripDetails['name'])
            ->setStartTime(new \DateTime($tripDetails['startTime']))
            ->setDuration($tripDetails['duration'])
            ->setRegistrationDeadline(new \DateTime($tripDetails['registrationDeadline']))
            ->setMaxRegistrations($tripDetails['maxRegistrations'])
            ->setInformation($tripDetails['information'])
            ->setState($state[0]);

        $entityManager->persist($trip[0]);
        $entityManager->flush();

        return $this->apiService->createJsonResponse("Les informations ont été mises à jour !", Response::HTTP_OK);
    }


    /**
     * Supprime la sortie spécifiée dans requête
     *
     * @param Request $request
     * @param TripRepository $tripRepository
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    #[Route('/api/trip/delete', name: 'api_trip_delete', methods: ['POST', 'OPTIONS'])]
    public function delete(Request $request, TripRepository $tripRepository, EntityManagerInterface $entityManager): JsonResponse
    {
        // Décode les données du front
        $data = json_decode($request->getContent(), true);

        $trip = $tripRepository->findBy(["name" => $data["tripName"]]);
        $organizer = $trip[0]->getOrganizer();

        // Vérifie que le participant ne peut supprimer que ses sorties
        if (!$this->tokenService->compareEmailFromToken($organizer->getEmail(), $data['token'])) {
            return $this->apiService->createJsonResponse("Cette sortie ne vous appartient pas", Response::HTTP_UNAUTHORIZED);
        }

        $entityManager->remove($trip[0]);
        $entityManager->flush();

        return $this->apiService->createJsonResponse("La sortie " . $data["tripName"] . " a été supprimée", Response::HTTP_OK);
    }
}
