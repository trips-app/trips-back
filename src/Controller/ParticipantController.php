<?php

namespace App\Controller;

use App\Repository\ParticipantRepository;
use App\Repository\SiteRepository;
use App\Service\ApiService;
use App\Service\TokenService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ParticipantController extends AbstractController
{
    private TokenService $tokenService;
    private ApiService $apiService;

    public function __construct(TokenService $tokenService, ApiService $apiService)
    {
        $this->tokenService = $tokenService;
        $this->apiService = $apiService;
    }


    /**
     * Renvoie la liste des participants avec leurs informations
     *
     * @param ParticipantRepository $participantRepository
     * @return JsonResponse
     */
    #[Route('/api/participant/list', name: 'api_participant_list', methods: ['POST', 'OPTIONS'])]
    public function list(ParticipantRepository $participantRepository): JsonResponse
    {
        // Récupère la liste des participants
        $participantList = $participantRepository->findAll();

        // Récupère la liste des participants sans la boucle infini entre Participant et Trip
        $participantListCleaned = [];
        foreach ($participantList as $participant) {
            $participantCleaned = $participant->toArray();
            $participantListCleaned[] = $participantCleaned;
        }

        return $this->apiService->createJsonResponse($participantListCleaned, Response::HTTP_OK);
    }


    /**
     * Génère et renvoie un JWT au participant qui se connecte
     *
     * @param Request $request
     * @param ParticipantRepository $participantRepository
     * @return JsonResponse
     */
    #[Route('/api/login', name: 'api_login', methods: ['POST', 'OPTIONS'])]
    public function login(Request $request, ParticipantRepository $participantRepository): JsonResponse
    {
        // Décode les données du front
        $data = json_decode($request->getContent(), true);
        $participantFromDB = $participantRepository->findOneBy(['email' => $data['email']]);

        if ($participantFromDB) {
            // Génére un token JWT pour l'utilisateur
            $token = $this->tokenService->createToken($participantFromDB);

            $response = [
                'message' => 'OK',
                'token' => $token,
            ];
        } else {
            $response = 'NOK';
        }

        return $this->apiService->createJsonResponse($response, Response::HTTP_OK);
    }


    /**
     * Renvoie les données du participant spécifié dans la requête
     *
     * @param Request $request
     * @param ParticipantRepository $participantRepository
     * @return JsonResponse
     */
    #[Route('/api/participant/detail', name: 'api_participant_detail', methods: ['GET'])]
    public function detail(Request $request, ParticipantRepository $participantRepository): JsonResponse
    {
        // Décode les données du front
        $data = json_decode($request->getContent(), true);

        $participant = $participantRepository->findBy(["email" => $data["email"]]);

        return $this->apiService->createJsonResponse($participant[0]->toArray(), Response::HTTP_OK);
    }


    /**
     * Modifie le participant avec les informations de la requête
     *
     * @param Request $request
     * @param ParticipantRepository $participantRepository
     * @param EntityManagerInterface $entityManager
     * @param SiteRepository $siteRepository
     * @return JsonResponse
     */
    #[Route('/api/participant/edit', name: 'api_participant_edit', methods: ['POST', 'OPTIONS'])]
    public function edit(Request $request, ParticipantRepository $participantRepository, EntityManagerInterface $entityManager, SiteRepository $siteRepository): JsonResponse
    {
        // Décode les données du front
        $data = json_decode($request->getContent(), true);

        // Vérifie que le participant ne peut modifier que son profil
        if (!$this->tokenService->compareEmailFromToken($data['email'], $data['token'])) {
            return $this->apiService->createJsonResponse("Vous ne pouvez pas modifier les informations de ce paticipant", Response::HTTP_UNAUTHORIZED);
        }

        // Vérifie que le participant existe en BDD
        $participant = $participantRepository->findOneBy(['email' => $data['email']]);
        if (!$participant) {
            return $this->apiService->createJsonResponse("Aucun participant ne possède ce mail", Response::HTTP_NOT_FOUND);
        }

        $site = $siteRepository->findBy(["name" => $data['participantDetails']['site']]);
        if (!$site) {
            return $this->apiService->createJsonResponse("Site inexistant", Response::HTTP_NOT_FOUND);  // Add a relevant message in French
        }

        // TODO : Gérer la modification du mail qui demande de générer un nouveau token
        $participant
            ->setFirstName($data['participantDetails']['firstName'])
            ->setLastName($data['participantDetails']['lastName'])
            ->setPhoneNumber($data['participantDetails']['phoneNumber'])
            ->setEmail($data['participantDetails']['email'])
            ->setPassword($data['participantDetails']['password'])
            ->setSite($site[0]);

        $entityManager->persist($participant);
        $entityManager->flush();

        return $this->apiService->createJsonResponse("Les informations ont été mises à jour !", Response::HTTP_OK);
    }
}
