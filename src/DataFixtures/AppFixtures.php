<?php

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Participant;
use App\Entity\Place;
use App\Entity\Site;
use App\Entity\State;
use App\Entity\Trip;
use App\Repository\StateRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use function Symfony\Component\Translation\t;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $this->addStates($manager);
        $this->addCities(10, $manager);
        $this->addPlaces(20, $manager);
        $this->addSites($manager);
        $this->addParticipants(30, $manager);
    }

    private function addStates(ObjectManager $manager)
    {
        $stateLibelle = ['Created', 'Opened', 'Closed', 'Ongoing', 'Passed', 'Canceled'];

        foreach ($stateLibelle as $libelle) {
            $state = new State();
            $state->setLibelle($libelle);

            $manager->persist($state);
        }

        $manager->flush();
    }

    private function addCities(int $number, ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < $number; $i++) {
            $city = new City();
            $city
                ->setName($faker->city)
                ->setPostalCode($faker->postcode);

            $manager->persist($city);
        }

        $manager->flush();
    }

    private function addPlaces(int $number, ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $cities = $manager->getRepository(City::class)->findAll();

        for ($i = 0; $i < $number; $i++) {
            $place = new Place();
            $place
                ->setName($faker->streetName)
                ->setStreet($faker->streetAddress)
                ->setLatitude($faker->latitude)
                ->setLongitude($faker->longitude)
                ->setCity($faker->randomElement($cities));

            $manager->persist($place);
        }

        $manager->flush();
    }

    private function addSites(ObjectManager $manager)
    {
        $eniSites = ['Rennes', 'Niort', 'Quimper', 'En ligne'];

        foreach ($eniSites as $site) {
            $newSite = new Site();
            $newSite->setName($site);

            $manager->persist($newSite);
        }

        $manager->flush();
    }

    private function addParticipants(int $number, ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $sites = $manager->getRepository(Site::class)->findAll();

        for ($i = 0; $i < $number; $i++) {
            $participant = new Participant();
            $participant
                ->setLastName($faker->lastName)
                ->setFirstName($faker->firstName)
                ->setPhoneNumber($faker->phoneNumber)
                ->setEmail($faker->email)
                ->setSite($faker->randomElement($sites))
                ->setPassword($faker->password);

            $manager->persist($participant);
            $manager->flush();

            $trip = $this->newTrip($participant, $manager);

            $participant->addTrip($trip);
        }
    }

    private function newTrip(Participant $participant, ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $states = $manager->getRepository(State::class)->findAll();
        $places = $manager->getRepository(Place::class)->findAll();
        $sites = $manager->getRepository(Site::class)->findAll();

        $trip = new Trip();

        $startTime = $faker->dateTime;
        $endTime = clone $startTime;
        $endTime->add(new \DateInterval('P1Y'));

        $trip
            ->setName($faker->jobTitle)
            ->setStartTime($startTime)
            ->setDuration($faker->time)
            ->setRegistrationDeadline($faker->dateTimeBetween($startTime, $endTime))
            ->setMaxRegistrations($faker->randomNumber())
            ->setInformation($faker->text)
            ->setState($faker->randomElement($states))
            ->setOrganizer($participant)
            ->setPlace($faker->randomElement($places))
            ->setSiteOrganization($faker->randomElement($sites));

        $manager->persist($trip);
        $manager->flush();

        return $trip;
    }
}
