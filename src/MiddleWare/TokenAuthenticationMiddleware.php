<?php

namespace App\MiddleWare;

use App\Service\TokenService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;

class TokenAuthenticationMiddleware
{
    private TokenService $tokenService;
    private $router;

    public function __construct(TokenService $tokenService, RouterInterface $router)
    {
        $this->tokenService = $tokenService;
        $this->router = $router;
    }


    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $request = $requestEvent->getRequest();
        $routeName = $request->get('_route');

        if ($routeName !== 'api_login' && $routeName !== 'api_participant_detail') {
            $data = json_decode($request->getContent(), true);

            // Vérifie la validité du token
            $tokenResponse = $this->tokenService->checkTokenValidity($data['token']);

            if ($tokenResponse["responseCode"] !== Response::HTTP_ACCEPTED) {
                $response = new Response('Token invalide', Response::HTTP_UNAUTHORIZED);
                $requestEvent->setResponse($response);
            }
        }
    }
}