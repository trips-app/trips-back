# Trips Back



## Commencer

Suivez les étapes suivantes pour installer et configurer le projet sur votre machine.

## Mettre en place WampServer

- [ ] [Lien de téléchargement](https://sourceforge.net/projects/wampserver/files/WampServer%203/WampServer%203.0.0/wampserver3.3.0_x64.exe/download)
- [ ] Changer la version de php à 8.2

## Mettre en place Composer

- [ ] [Lien de téléchargement](https://getcomposer.org/Composer-Setup.exe)
- [ ] Ajouter la version de php dans les variables d'environnement
- [ ] Vérifier que Composer est bien installé avec la commande ```composer -v```

## Mettre en place Symfony

- [ ] [Lien de téléchargement pour windows x64](https://github.com/symfony-cli/symfony-cli/releases/download/v5.6.1/symfony-cli_windows_amd64.zip)
- [ ] Ajouter le chemin du dossier dans la variable PATH des variables d'environnements
- [ ] Vérifier que Symfony est bien installé avec la commande ```symfony -v```

## Installer OpenSSL et créer les clés de chiffrements

- [ ] [Lien de téléchargement pour windows x64](https://slproweb.com/download/Win64OpenSSL_Light-3_0_11.exe)
- [ ] Ajouter le chemin du dossier /bin dans la variable PATH des variables d'environnements
- [ ] Ajouter le chemin du dossier /bin/openssl.cfg avec le nom "OPENSSL_CONFIG" les variables d'environnements pour Administrateur
- [ ] Vérifier que Openssl est bien installé avec la commande ```openssl version```
- [ ] Créer le dossier /config/jwt dans le projet
- [ ] Y créer la clé privée avec la commande ```openssl genpkey -algorithm RSA -out private.pem -aes256```
- [ ] Entrer la passphrase qui se trouve dans le fichier .env
- [ ] Y créer la clé publique avec la commande ```openssl rsa -in private.pem -pubout -out public.pem```
- [ ] Entrer la passphrase qui se trouve dans le fichier .env

## Créer et configurer la BDD

- [ ] Adapter l'url de la BDD dans le fichier .env.local si besoin
- [ ] Créer la BDD avec la commande ```symfony console d:d:c```
- [ ] Créer les tables la commande ```symfony console d:s:u --force```
- [ ] Créer les données la commande ```symfony console d:f:l```

Vous pouvez maintenant utiliser l'application en local via la commande ```symfony serve -d```
